from urllib.request import urlopen

# Source: https://stackoverflow.com/questions/16025368/download-file-as-string-in-python
# Source: https://stackoverflow.com/questions/52455077/python-access-multiple-elements-in-list
# Source: https://stackoverflow.com/questions/3437059/does-python-have-a-string-contains-substring-method

# download csv
url = 'http://winterolympicsmedals.com/medals.csv'
output = urlopen(url).read()

# create list from lines
lines = output.decode('utf-8').splitlines()

# create a list with all norwegian gold meddals
all_gold = []
for line in lines:
	if "NOR" and "Gold" in line:
		all_gold.append(line.split(","))

# filter the list to only show year, discipline and event
index = [0, 3, 5]
filter = []
for line in all_gold:
	temp = [line[i] for i in index]
	filter.append(temp)

# get the first line from the original csv, filter it and add to a new list
first_line = lines[0].split(",")
result = []

result.append([first_line[i] for i in index])

# add the filter-list to the result
for line in filter:
	result.append(line)

# print the result, ready to export to new csv-file
for line in result:
	print(*line, sep = ", ")